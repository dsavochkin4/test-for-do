const express = require('express');
const cors = require('cors');

const app = express();

app.use(cors());

app.get('/ping', (req, res) => {
    return res.status(200).json({ message: 'pong...' });
})

app.listen(3000, () => {
    console.log('Server started on port 3000');
})